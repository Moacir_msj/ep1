#ifndef URNA_HPP
#define URNA_HPP

#include <iostream>
#include <string>
#include "candidato.hpp"
#include "eleitor.hpp"
#include <iostream>
#include <stdio.h>
#include <string>
#include <fstream>
#include <cstring>


using namespace std;

class Urna{
	public:
		Candidato *governador, *senador1,*senador2,*presidente,*deputado_distrital, *deputado_federal;
		Eleitor *eleitor;

		Urna();
		~Urna();	


		void busca_dados(char nome_arquivo[100], string codigo, Candidato * candidato,string cargo_desejado);
		int busca_colunas(char nome_arquivo[100], string coluna);
		void votar_candidato(string cargo ,Candidato *candidato,char arquivo[100]);

};
#endif
