#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <iostream>
#include <string>

using namespace std;

class Pessoa{
protected:
	string nome;

public:
	Pessoa();
	~Pessoa();

string get_nome();
void set_nome(string nome);	

};

#endif