#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class Candidato:public Pessoa{
	private:
	string cargo;
	string UE;
	string numero;
	string partido;

	public:
	Candidato();
	~Candidato();


	string get_cargo();
	void set_cargo(string cargo);
	string get_UE();
	void set_UE(string UE);
	string get_numero();
	void set_numero(string numero);
	string get_partido();
	void set_partido(string partido);


};

#endif