#ifndef ELEITOR_HPP
#define ELEITOR_HPP
#include <iostream>
#include <string>
#include "pessoa.hpp"

using namespace std;

class Eleitor:public Pessoa{
	private:
		string numero;

	public:
		Eleitor();
		~Eleitor();

		string get_numero();
		void set_numero(string numero);		



};


#endif