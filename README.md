EP1-00 (Unb Gama 2018/2)

Projeto desenvolvido em c++ que visa reproduzir as funcionalidades de uma urna eletrônica;


Bibliotecas utilizadas:

iostream,stdlib.h,stdio.h,stdio_ext.h,math.h,string,
fstream,cstring,unistd.h,algorithm;


Funcionalidades:

loop de votação,votar em branco,cancelar voto,confirmar,registro de votos;
    
    
    O arquivo com o registro das votações será criado após o primeiro voto.     


Instruções:

    1.Clonar para o seu computador ou repositório ou baixar de forma compactada;

    2.Acessar o diretorio do projeto via terminal;

    3.Inserir o comando 'make clean'para limpar os arquivos objeto caso existam;

    4.Inserir o comando 'make' para compilar o programa;

    5.Inserir o comando 'make run' para iniciar o programa;


Dados do aluno:
Nome: Moacir Mascarenha Soares Junior
Matricula: 17/0080366
Repositório: https://gitlab.com/Moacir_msj/ep1