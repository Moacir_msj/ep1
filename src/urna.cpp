#include "urna.hpp"
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "validacoes.hpp"


Urna::Urna(){
	this->deputado_distrital = new Candidato();
	this->deputado_federal = new Candidato();
	this->senador1 = new Candidato();
	this->senador2 = new Candidato();
	this->governador = new Candidato();
	this->presidente = new Candidato();
	this->eleitor = new Eleitor();
}
Urna::~Urna(){}

	
void Urna::busca_dados(char nome_arquivo[100], string codigo, Candidato * candidato, string cargo_desejado){
	codigo = "\""+codigo+"\""; 
	cargo_desejado= "\""+cargo_desejado+"\"";
	std::string nome,partido,ue,numero,cargo;
	string informacao_candidato, palavra;
	ifstream arqv(nome_arquivo);
	int col_ue,col_nome,col_partido,col_numero,col_cargo, i=0,aux;

	col_numero =busca_colunas(nome_arquivo, "NR_CANDIDATO");
	col_cargo = busca_colunas(nome_arquivo, "DS_CARGO");
	col_nome = busca_colunas(nome_arquivo, "NM_URNA_CANDIDATO");
	col_ue = busca_colunas(nome_arquivo, "NM_UE");
	col_partido = busca_colunas(nome_arquivo, "NM_PARTIDO");



	if(!arqv.is_open()) cout <<"Erro ao abrir o arquivo!"<<endl;

	while(arqv.good()){
		getline(arqv,palavra,';');
		
		i++;

		if(i== col_numero){
			numero=palavra;
			if(numero == codigo){
				aux=1;
			}
			col_numero+=57;
		}
		else if(i == col_cargo){
			cargo=palavra;
			col_cargo +=57;
		}
		else if(i==col_nome){
			nome=palavra;
			col_nome+=57;
			
		}
		else if(i==col_partido ){
			partido=palavra;
			if(codigo == numero && cargo ==cargo_desejado){
				candidato->set_nome(nome);
				candidato->set_cargo(cargo);
				candidato->set_partido(partido);
				candidato->set_numero(numero);
			}
			aux=0;
			col_partido+=57;
			
		}
		else if(i == col_ue){
			ue = palavra;
			col_ue+=57;
			
		}
		
			
	}

arqv.close();


}



int Urna::busca_colunas(char nome_arquivo[100], string coluna){
	int aux = 0;
	coluna = "\""+coluna+"\"";
	string palavra;
	ifstream arq(nome_arquivo);

	if(!arq.is_open()){
		cout <<"Erro ao abrir o arquivo!"<<endl;
	} 

	while(arq.good()){
		getline(arq, palavra,';');

		aux++;

		if(palavra == coluna){
		return (aux);
	}
		
		

	}
	arq.close();

	return 0;
}


void Urna::votar_candidato(string cargo ,Candidato *candidato,char arquivo[100]){
	string numero , opcao , nome ,partido ,numero_cand;
	int op, aux;
	
	do{
		system("clear");
		cout <<cargo<<endl;
		cout << "DESEJAR VOTAR EM BRANCO?\n1 SIM 2 NÃO: ";
		cin >> op;
		op = intervalo(op,0,3);

		if(op == 1){
			opcao = "BRANCO";
			candidato->set_nome(opcao);
			candidato->set_numero(opcao);
			candidato->set_partido(opcao);
			candidato->set_cargo(opcao);
		}
		else{	
			system("clear");
			cout <<"NUMERO DO "<<cargo<<": ";
			cin >>numero;
			this->busca_dados(arquivo, numero,candidato,cargo);
			
			if(candidato->get_nome()== ""){
				system("clear");
				cout <<cargo<<" NÃO ENCONTRADO!"<<endl;
				op=3;
				sleep(4);

			}

			else{
				system("clear");
				
				nome= candidato->get_nome();
				numero_cand=candidato->get_numero();
				partido =candidato->get_partido();
				nome = remove_aspas(nome);
				numero_cand = remove_aspas(numero_cand);
				partido = remove_aspas(partido);
				
				cout << nome<<endl;
				cout << numero_cand<<endl;
				cout << partido<<endl;

				cout << "\n1: CONFIRMAR\t2: BRANCO\t 3: CANCELAR\n";
				cin >>op;
				op = intervalo(op,0 ,4);

				if(op == 2){
					opcao = "BRANCO";
					candidato->set_nome(opcao);
					candidato->set_numero(opcao);
					candidato->set_partido(opcao);
					candidato->set_cargo(opcao);
				}
				else if(op==3){
					candidato->set_nome("");
					candidato->set_numero("");
					candidato->set_partido("");
					candidato->set_cargo("");
				}
			}
		}
		system("clear");
	}while(op==3);
}




