#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "urna.hpp"
#include <unistd.h>
#include <stdio_ext.h>
#include <math.h>



#define ARQUIVO_BR "./doc/consulta_cand_2018_BR.csv"  
#define ARQUIVO_DF "./doc/consulta_cand_2018_DF.csv"
using namespace std;


int somente_num(int num);
void registra_votacao(Eleitor *eleitor , Candidato * deputado_distrital, Candidato * deputado_federal,Candidato *senador1,Candidato *senador2, Candidato *governador,Candidato *presidente);

int main(){
	
	int numero_de_eleitores;
	string nome, titulo_de_eleitor;
	Urna * urna = new Urna();

	
	system("clear");
	cout << "NUMERO DE ELEITORES: ";
	numero_de_eleitores = somente_num(numero_de_eleitores);

	while(numero_de_eleitores != 0){
		system("clear");
		cout << "NOME: ";
		cin >> nome;
		urna->eleitor->set_nome(nome);
		cout << "NUMERO TITULO: ";
		cin >> titulo_de_eleitor;
		urna->eleitor->set_numero(titulo_de_eleitor);
		urna->votar_candidato("DEPUTADO DISTRITAL",urna->deputado_distrital, ARQUIVO_DF);
		urna->votar_candidato("DEPUTADO FEDERAL",urna->deputado_federal, ARQUIVO_DF);
		urna->votar_candidato("SENADOR",urna->senador1, ARQUIVO_DF);
		urna->votar_candidato("SENADOR",urna->senador2, ARQUIVO_DF);
		urna->votar_candidato("GOVERNADOR",urna->governador, ARQUIVO_DF);
		urna->votar_candidato("PRESIDENTE",urna->presidente, ARQUIVO_BR);
	
		registra_votacao(urna->eleitor,urna->deputado_distrital,urna->deputado_federal,urna->senador1,urna->senador2,urna->governador,urna->presidente);
		numero_de_eleitores--;
		cout << "VOTO REALIZADO COM SUCESSO!!"<<endl;
		sleep(2);
	} 
	system("clear");
	cout <<"\n\n\n\n\t\t\tFIM DA VOTAÇÃO!!!\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
	
	free(urna->deputado_distrital);
	free(urna->deputado_federal);
	free(urna->senador1);
	free(urna->senador2);
	free(urna->governador);
	free(urna->presidente);
	free(urna->eleitor);
	free(urna);
	return 0;
}


int somente_num(int num){
	int verificador;
	verificador = scanf("%i", &num);
	while(verificador == 0){
		cout << "ERRO !DIGITE SOMENTE NUMEROS!"<<endl;
		 __fpurge(stdin);
		verificador = scanf("%i", &num);
	system("clear");
	}
	return fabs(num);
}



void registra_votacao(Eleitor *eleitor , Candidato * deputado_distrital, Candidato * deputado_federal,Candidato *senador1,Candidato *senador2, Candidato *governador,Candidato *presidente){

	std::fstream arqv;
	arqv.open ("registro_votacao.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	arqv <<"ELEITOR: "<< eleitor->get_nome()<<endl;
	arqv <<"TITULO: " << eleitor->get_numero()<<endl;
	arqv << "VOTO:----------------------------------------"<<endl;
	arqv<<"DEPUTADO DISTRITAL:--------\nNOME: "<<deputado_distrital->get_nome()<<" ";
	arqv<<"PARTIDO: " <<deputado_distrital->get_partido()<<" ";
	arqv <<"NUMERO: " <<deputado_distrital->get_numero()<<endl;
	arqv<<"DEPUTADO FEDERAL:--------\nNOME: "<<deputado_federal->get_nome()<<" ";
	arqv<<"PARTIDO: " <<deputado_federal->get_partido()<<" ";
	arqv <<"NUMERO: " <<deputado_federal->get_numero()<<endl;
	arqv<<"SENADOR:--------\nNOME: "<<senador1->get_nome()<<" ";
	arqv<<"PARTIDO: " <<senador1->get_partido()<<" ";
	arqv <<"NUMERO: " <<senador1->get_numero()<<endl;
	arqv<<"SENADOR:--------\nNOME: "<<senador2->get_nome()<<" ";
	arqv<<"PARTIDO: " <<senador2->get_partido()<<" ";
	arqv <<"NUMERO: " <<senador2->get_numero()<<endl;
	arqv<<"GOVERNADOR:--------\nNOME: "<<governador->get_nome()<<" ";
	arqv<<"PARTIDO: " <<governador->get_partido()<<" ";
	arqv <<"NUMERO: " <<governador->get_numero()<<endl;
	arqv<<"PRESIDENTE:--------\nNOME: "<<presidente->get_nome()<<" ";
	arqv<<"PARTIDO: " <<presidente->get_partido()<<" ";
	arqv <<"NUMERO: " <<presidente->get_numero()<<endl;
	arqv<< "--------------------------------------------------------------------\n";
  	arqv.close();
}			


